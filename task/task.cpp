#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

const char* cities[] = {
    "���",
    "�������",
    "��������-����������",
    "г���",
    "�����",
    "��������",
    "³�����",
    "������������",
    "��������",
    "��������",
    "���� ������",
    "�����",
    "�������",
    "���������",
    "�������",
    "�����",
    "�������",
    "����",
    "��������"
};

int matrix[19][19] = { 0 }; // ³������ �� ������

// ����� � ������ ��� ����������� �������� ��������
void BFS() {
    int route[20] = { 0 };
    bool visited[20] = { false };

    int current = 0;
    visited[current] = true;

    int queue[20], front = 0, rear = -1; // ����� ��� ���������� ��� � ������� ������
    queue[++rear] = current;

    while (front <= rear) {
        current = queue[front++];

        // �������� ������� ����, �� ����� ������� � ��������� ����
        for (int i = 0; i < sizeof(cities) / sizeof(cities[0]); i++) {
            if (matrix[current][i] != 0 && !visited[i]) {
                visited[i] = true;
                queue[++rear] = i;
                route[i] = route[current] + matrix[current][i]; // ³������ ��������
                printf("��� - %s (%d��)\n", cities[i], route[i]); // ��������� ��������
            }
        }
    }
}

int path = 0;

// ����� � ������� ��� ����������� ��� �������� ��������
void DFS(int current, const char* route, int startingCity) {
    char newRoute[200];
    if (current != 0) {
        sprintf(newRoute, "%s - %s", route, cities[current]); // ���������� ������ ��������
        path += matrix[startingCity][current]; // ��������� ������� �� ��������
        printf("%s (%d��)\n", newRoute, path); // ��������� ��������
    }
    else {
        strcpy(newRoute, cities[current]); // ��������� ���� ��������
    }

    // ����������� ������ ��� ������� ��������� ����
    for (int i = 0; i < sizeof(cities) / sizeof(cities[0]); i++) {
        if (matrix[current][i] != 0) {
            DFS(i, newRoute, current);
            path -= matrix[current][i]; // ��������� ������� �� ������
        }
    }
}

int main() {
    system("chcp 1251"); // ������� � ������ �� ��������
    system("cls"); // ������� ���� ������

    matrix[0][1] = 135;
    matrix[1][2] = 80;
    matrix[2][3] = 100;
    matrix[3][4] = 68;
    matrix[1][5] = 38;
    matrix[5][6] = 73;
    matrix[6][7] = 110;
    matrix[7][8] = 104;
    matrix[1][9] = 115;
    matrix[0][10] = 78;
    matrix[10][11] = 115;
    matrix[10][12] = 146;
    matrix[12][13] = 105;
    matrix[10][14] = 181;
    matrix[14][15] = 130;
    matrix[0][16] = 128;
    matrix[16][17] = 175;
    matrix[16][18] = 109;

    printf("������ �������� ����:\n");
    BFS();

    printf("\n��������:\n");
    DFS(0, "", 0);

    return 0;
}

